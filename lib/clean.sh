#!/bin/bash
local_path="${0//clean.sh/}"
cd ${local_path}..
echo "This script will delete all the data, are you sure ? [y/N]"
read answer
if [[ $answer == "y" ]] || [[ $answer == "Y" ]];then
	echo "Cleaning..."
	rm -f ./traefik/letsencrypt/acme.json
	for i in $(docker volume ls | grep moodle | awk '{print $2}');do docker volume rm $i;done
	for i in $(docker volume ls | grep trax | awk '{print $2}');do docker volume rm $i;done
	for i in $(docker volume ls | grep traefik | awk '{print $2}');do docker volume rm $i;done
	echo "" > ./traefik/log/access.log
	echo "" > ./traefik/log/traefik.log
	sed -i 's/email.*/email = "\&acme_mail\&"/g' ./traefik/traefik.toml
	rm -f docker-compose.yml
fi