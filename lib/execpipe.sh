#!/bin/bash

if [ -z "$1" ]; then
  RED='\033[0;31m'
  NC='\033[0m' # No Color
  echo -e "${RED}Missing pipe directory parameter${NC}"
  exit 1
else
  echo "Installing a named pipe for command execution in $1"
fi

mkdir -p "$1"

pipe="$1/pipe"
if [ ! -p $pipe ]; then
  echo "Creating pipe $pipe"
  mkfifo $pipe
fi

echo "Listening for commands in pipe $pipe"
while true;
  do
    eval "$(cat $pipe)" &> $pipe.log ;
  done
