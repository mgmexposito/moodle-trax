#!/bin/bash

source ""$(dirname "$(dirname "$(readlink -fm "$0")")")"/.env"
echo "Moodle version = $MDL_VERSION"
echo "Enable Trax = $ENABLE_TRAX"
if [ "$MDL_VERSION" = "39" ] && [ "$ENABLE_TRAX" = "1" ];then
  echo "Installing Trax plugins"

  if [ -z ${MDL_VOLUME_BIND_CODE+x} ]; then # $MDL_VOLUME_BIND_CODE is not set -> default volumes
    # TODO : get the mount point source depending on its destination : /var/www/html/modle (by parsing inspect results)
    MOODLE_CODE_DIR="/var/lib/docker/volumes/moodle-trax_moodle_code/_data"
  else
    MOODLE_CODE_DIR=$MDL_VOLUME_BIND_CODE
  fi

  if ! sudo test -d $MOODLE_CODE_DIR
  then
      echo "$MOODLE_CODE_DIR does not exists on your filesystem."
      exit 1
  fi

  echo "Installing trax log plugin"
  if ! sudo test -d $MOODLE_CODE_DIR/admin/tool/log/store/trax ; then
    sudo curl https://codeload.github.com/trax-project/moodle-trax-logs/zip/refs/tags/v$TRAX_LOG_VERSION -o $MOODLE_CODE_DIR/admin/tool/log/store/trax.zip
    sudo unzip -q $MOODLE_CODE_DIR/admin/tool/log/store/trax.zip -d $MOODLE_CODE_DIR/admin/tool/log/store/
    sudo rm /$MOODLE_CODE_DIR/admin/tool/log/store/trax.zip
    sudo mv $MOODLE_CODE_DIR/admin/tool/log/store/moodle-trax-logs-${TRAX_LOG_VERSION} $MOODLE_CODE_DIR/admin/tool/log/store/trax
    sudo chown -R www-data:www-data $MOODLE_CODE_DIR/admin/tool/log/store/trax/

    ENDPOINT_PASSWORD=$(echo $(uuidgen) | sed -r 's/[\-]+//g')
    ENDPOINT_UUID=$(uuidgen)
    DIR_PATH=$(dirname $(realpath $0))

    cp $DIR_PATH/logstore_trax_conf_v020.sql $DIR_PATH/temp_moodle.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'version', '2020060900');" >> $DIR_PATH/temp_moodle.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'platform_iri', 'http://$MOODLE_DN');" >> $DIR_PATH/temp_moodle.sql
    # Hostname d'accès à Trax : trax = le nom du container dans le réseau Moodle-Trax
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_endpoint', 'http://trax/trax/api/$ENDPOINT_UUID/xapi/std/');" >> $DIR_PATH/temp_moodle.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_username', 'moodle');" >> $DIR_PATH/temp_moodle.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_password', '$ENDPOINT_PASSWORD');" >> $DIR_PATH/temp_moodle.sql
    docker cp $DIR_PATH/temp_moodle.sql moodle_db_local:/tmp/temp_moodle.sql
    docker exec -it moodle_db_local sh -c "export PGPASSWORD=$MDL_DB_PASS && psql --username $MDL_DB_USER --dbname $MDL_DB_NAME -f /tmp/temp_moodle.sql"
    rm $DIR_PATH/temp_moodle.sql

    touch $DIR_PATH/temp_trax.sql
    echo "INSERT INTO public.trax_owners (uuid, name, meta, created_at, updated_at)VALUES ('$(uuidgen)', 'Default Store', '[]', now(), now());" >> $DIR_PATH/temp_trax.sql
    echo "INSERT INTO public.trax_clients (name, active, meta, permissions, admin, created_at, updated_at, entity_id, owner_id)VALUES ('moodle', true, '[]', '{ \"xapi-scope.all\": true}', false, now(), now(), null, 1);" >> $DIR_PATH/temp_trax.sql
    echo "INSERT INTO public.trax_basic_http (username, password) VALUES ('moodle', '$ENDPOINT_PASSWORD');" >> $DIR_PATH/temp_trax.sql
    echo "INSERT INTO public.trax_accesses (uuid, name, cors, active, meta, permissions, admin, inherited_permissions, created_at, updated_at, credentials_id, credentials_type, client_id)VALUES ('$ENDPOINT_UUID', 'moodle', '*', true, '[]', '[]', false, true, now(), now(), 1, 'Trax\Auth\Stores\BasicHttp\BasicHttp', 1);" >> $DIR_PATH/temp_trax.sql
    docker cp $DIR_PATH/temp_trax.sql trax_db_local:/tmp/temp_trax.sql
    docker exec -it trax_db_local sh -c "export PGPASSWORD=$TRAX_DB_PASS && psql --username $TRAX_DB_USER --dbname $TRAX_DB_NAME -f /tmp/temp_trax.sql"
    rm $DIR_PATH/temp_trax.sql

    # Trax logs plugin activation
    docker exec -it moodle_db_local sh -c "export PGPASSWORD=$MDL_DB_PASS && psql --username $MDL_DB_USER --dbname $MDL_DB_NAME -c \"update mdl_config_plugins set value ='logstore_standard,logstore_trax' where name = 'enabled_stores'; WITH config_log AS( insert into mdl_config_log (userid, timemodified, name, oldvalue, value, plugin) VALUES (2, extract(epoch from now()), 'tool_logstore_visibility', 0, 1, 'logstore_trax') RETURNING id) INSERT INTO mdl_logstore_standard_log (eventname, component, action, target, objecttable, objectid, crud, edulevel, contextid, contextlevel, contextinstanceid, userid, courseid, relateduserid, anonymous, other, timecreated, origin, ip, realuserid) VALUES ('\core\event\config_log_created', 'core', 'created', 'config_log', 'config_log', (SELECT id from config_log), 'c', '0', '1', '10', '0', '2', '0', NULL, '0', '{\"name\":\"tool_logstore_visibility\",\"oldvalue\":\"0\",\"value\":\"1\",\"plugin\":\"logstore_trax\"}', extract(epoch from now()), 'web', '127.0.0.1', NULL) ;\""
    docker exec -w /var/www/html/moodle -it moodle_local sh -c "php admin/cli/purge_caches.php"
  else
      echo "Trax log plugin already installed"
  fi

  echo "Installing trax video plugin"
  if sudo test ! -d $MOODLE_CODE_DIR/mod/traxvideo ; then
    sudo curl https://codeload.github.com/trax-project/moodle-trax-video/zip/refs/tags/v$TRAX_VIDEO_VERSION -o $MOODLE_CODE_DIR/mod/traxvideo.zip
    sudo unzip -q $MOODLE_CODE_DIR/mod/traxvideo.zip -d $MOODLE_CODE_DIR/mod/
    sudo rm $MOODLE_CODE_DIR/mod/traxvideo.zip
    sudo mv $MOODLE_CODE_DIR/mod/moodle-trax-video-0.4 $MOODLE_CODE_DIR/mod/traxvideo
    sudo chown -R www-data:www-data $MOODLE_CODE_DIR/mod/traxvideo/
  else
    echo "Trax video plugin already installed"
  fi

else
  echo "No plugins to install"
fi