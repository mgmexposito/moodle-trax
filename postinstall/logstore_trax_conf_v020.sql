-- INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'version', '2020060900');
-- INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_endpoint', 'http://trax/trax/api/b8c086db-cd04-4f69-8954-b483f92a32d5/xapi/std/');
-- INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_username', 'username');
-- INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_password', 'password');
-- INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'platform_iri', 'http://my.lms');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'actors_identification', '0');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'xis_anonymization', '1');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'firstlogs', TO_CHAR(NOW() :: DATE, 'dd/mm/yyyy'));
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'core_events', 'management,authentication,navigation,completion,grading');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'moodle_components', 'mod_assign,mod_book,mod_chat,mod_choice,mod_data,mod_feedback,mod_folder,mod_forum,mod_glossary,mod_imscp,mod_lesson,mod_lti,mod_page,mod_quiz,mod_resource,mod_scorm,mod_survey,mod_url,mod_wiki,mod_workshop,mod_h5pactivity');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'additional_components', 'other');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'scheduled_statements', 'define_groups,define_courses');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'resend_livelogs_until', TO_CHAR(NOW() :: DATE, 'dd/mm/yyyy'));
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'sync_mode', '0');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'attempts', '1');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'db_batch_size', '100');
INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'xapi_batch_size', '10');

create table mdl_logstore_trax_actors
(
    id bigserial
        constraint mdl_logstraxacto_id_pk
            primary key,
    mid bigint not null,
    email varchar(100),
    type varchar(50) default ''::character varying not null,
    uuid varchar(36) default ''::character varying not null
);

comment on table mdl_logstore_trax_actors is 'Actors table';

create unique index mdl_logstraxacto_midtyp_uix
    on mdl_logstore_trax_actors (mid, type);

create unique index mdl_logstraxacto_uui_uix
    on mdl_logstore_trax_actors (uuid);

create table mdl_logstore_trax_activities
(
    id bigserial
        constraint mdl_logstraxacti_id_pk
            primary key,
    mid bigint not null,
    type varchar(50) default ''::character varying not null,
    uuid varchar(36) default ''::character varying not null
);

comment on table mdl_logstore_trax_activities is 'Activities table';

create unique index mdl_logstraxacti_midtyp_uix
    on mdl_logstore_trax_activities (mid, type);

create index mdl_logstraxacti_uui_ix
    on mdl_logstore_trax_activities (uuid);

create table mdl_logstore_trax_logs
(
    id bigserial
        constraint mdl_logstraxlogs_id_pk
            primary key,
    mid bigint,
    error smallint default 0 not null,
    attempts smallint default 1 not null,
    newattempt smallint default 0 not null
);

comment on table mdl_logstore_trax_logs is 'Logs table';

create unique index mdl_logstraxlogs_mid_uix
    on mdl_logstore_trax_logs (mid);

create index mdl_logstraxlogs_err_ix
    on mdl_logstore_trax_logs (error);

create index mdl_logstraxlogs_att_ix
    on mdl_logstore_trax_logs (attempts);

create index mdl_logstraxlogs_new_ix
    on mdl_logstore_trax_logs (newattempt);

create table mdl_logstore_trax_status
(
    id bigserial
        constraint mdl_logstraxstat_id_pk
            primary key,
    event varchar(50) default ''::character varying not null,
    objecttable varchar(50),
    objectid bigint,
    data text not null
);

comment on table mdl_logstore_trax_status is 'Logs status';


