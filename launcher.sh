#!/bin/bash
local_path="$(realpath ${0//launcher.sh/})"
# shellcheck disable=SC2086
# shellcheck disable=SC1090
source ${local_path}/lib/lib.sh
source ${local_path}/.env 2> /dev/null

arg_list=('docker_install' 'build' 'start' 'stop' 'config' 'backup' 'prepare')
# shellcheck disable=SC2068
for i in ${arg_list[@]};do
	if [[ $(echo $@ | grep ".*$i.*") != "" ]];then
		method=$i
		# shellcheck disable=SC2046
		set -- $(echo "$@" | sed -E "s/ ?$i ?//g")
		break
	elif [[ $i == "${arg_list[-1]}" ]];then
		usage
		exit 1
	fi
done
while getopts "hvf" option;do
	case $option in
		h)
			usage
			exit 0
		;;
		v)
			# shellcheck disable=SC2034
			verbose="True"
		;;
    f)
      force="True"
    ;;
    *)
      echo "Unknow option, ignoring it"
    ;;
	esac;
done

if [[ $(id -u) -eq 0 ]] && [[ $force != "True" ]];then
	echo "Don't run with root privileges"
	usage
	exit 1
fi

case $method in
  "build")
    build_image
  ;;
  "start")
    start_container
  ;;
  "stop")
    stop_container
  ;;
  "docker_install")
    docker_install
  ;;
  "config")
    configure
  ;;
  "backup")
    backup
  ;;
  "prepare")
    prepare_env
  ;;
esac
