#!/bin/bash

source ""$(dirname "$(dirname "$(readlink -fm "$0")")")"/.env"
echo "Moodle version = $MDL_VERSION"
if [ "$MDL_VERSION" = "39" ]; then
  echo "Moodle configuration finalization"
  docker exec -it moodle_db_local sh -c "export PGPASSWORD=$MDL_DB_PASS && psql --username $MDL_DB_USER --dbname $MDL_DB_NAME -c \"update mdl_user set email = '$MDL_ADMIN_EMAIL' where id = 2; update mdl_course  set shortname    = '$MDL_SHORT_NAME', fullname= '$MDL_FULL_NAME', timemodified = extract(epoch from now())  where id = 1;  insert into mdl_config (name, value)  values ('noreplyaddress', '$MDL_NOREPLY');  update mdl_config  set value = '0'  where name = 'registrationpending';  WITH config_log AS ( INSERT INTO mdl_config_log (userid, timemodified, name, oldvalue, value, plugin)VALUES ('2', extract(epoch from now()), 'noreplyaddress',   NULL, '$MDL_NOREPLY', NULL) RETURNING id  )  INSERT  INTO mdl_logstore_standard_log (eventname, component, action, target, objecttable, objectid, crud, edulevel,    contextid, contextlevel, contextinstanceid, userid, courseid, relateduserid,    anonymous, other, timecreated, origin, ip, realuserid)  VALUES ('\core\event\config_log_created', 'core', 'created', 'config_log', 'config_log',  (SELECT id from config_log), 'c', '0', '1','10', '0', '2', '0', NULL, '0','{\"name\":\"noreplyaddress\",\"oldvalue\":null,\"value\":\"$MDL_NOREPLY\",\"plugin\":null}',extract(epoch from now()), 'web', '127.0.0.1', NULL);\""
fi
