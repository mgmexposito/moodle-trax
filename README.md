# docker-moodletrax

Ce logiciel permet d'installer un ensemble de conteneurs Docker : Moodle, Trax et ELK.
Les liaisons Moodle -> Trax et Trax -> ELK sont configurées automatiquement.

Versions supportées :
- Moodle 3.9
- Trax 2 beta 5
- ELK 7.15.1

Prérequis : installation Docker, environnement Unix.
Les scripts bash ne sont pas compatibles Mac.

**Attention : le script launcher.sh ne doit pas être lancé en root, mais avec un compte sudoer.**

## Utilisation manuelle sur Ubuntu

Installation de docker :
````shell
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose
sudo usermod -aG docker $USER
newgrp docker
````

Copier le example.env en .env, et le modifier.

Gérérer le fichier de configuration, et construire les images :
```shell
source .env
./launcher.sh config
./launcher.sh build
docker network create ext_traefik
docker-compose up -d --build
```

Finalisation de la cnnfiguration Moodle :
```shell
./postinstall/moodle_config.sh
```

Installation des plugins Trax dans Moodle :
```shell
./postinstall/plugins_install.sh
```

## Utilisation par le script laucher.sh

### Configuration de l'instance moodle+trax
````shell
cp example.env .env
````
Editer le .env.
````shell
./launcher.sh config
````
Le .env sera utilisé lors du démarrage des conteneurs Docker (launcher start).

### Installation de l'instance moodle+trax en local
1. Préparer l'installation des prérequis (Git, Fail2ban) :
````shell
./launcher.sh prepare
````

2. Installer Docker :
````shell
./launcher.sh docker_install
````

3. Préparation des images :
````shell
./launcher.sh build
````

4. Script de préparation de la configuration du déployement des containers :
````shell
./launcher.sh config
````
**OU** manuellement, pour remplacer les interactions en ligne de commande par l'édition d'un ficher :
````shell
cp example.env .env
````
Editer le .env, puis lancer :
````shell
./launcher.sh config
````
Editer le .env.

5. Lancement des containers :
````shell
./launcher.sh start
````

## Backup
The backup configuration is read from .backup-secrets file, you can copy backup-secrets.example and fill it.
If no cipherkey is entered, the backup files will not be ciphered

## Notes

### Préparation d'un docker sur machine distante
On considère une machine distante en Centos 7.

Sur la machine distante :
1. Désactiver SELinux ou vérifier la configuration.
2. Effectuer les actions de préparation (launcher.sh prepare), sauf fail2ban :
```shell
./launcher.sh docker_install
```
3. Activer le point d'accès TCP à l'API Docker :
```shell
systemctl edit docker
```

```ini
[Service]
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
```
```shell
systemctl daemon-reload
systemctl restart docker
```

4. Ouvrir le parefeu :
```shell
firewall-cmd --zone=public --add-port=2375/tcp --permanent
firewall-cmd --reload
```

5. Vérifier que docker écoute sur le port 2375 en ipv4 sur 0.0.0.0.

On utilise Docker dans WSL2 pour s'exécuter sur un serveur distant (image HyperV). Problème : WSL2 et HyperV sont dans des réseaux locaux différents. On va donc devoir utiliser un tunnel.

6Récup IP de l'hôte local WSL2 (IP du poste Windows dans le réseau local de WSL2) : 
````shell
cat /etc/resolv.conf `| grep nameserver `| cut -d ' ' -f 2
````

7. Création du tunnel de l'hôte local WSL2 vers la VM HyperV (elevated PowerShell) :
````shell
netsh interface portproxy add v4tov4 listenport=2375 listenaddress=[IP hôte local WSL] connectport=2375 connectaddress=[IP VM HyperV]
````

8. Dans le parefeu Windows, ouvrir le port TCP 2375.

9. Sur la machine locale, configurer l'accès Docker dans Intellij :
````
TCP Socket :
    Engine API URL = tcp://[IP VM HyperV]:2375
    Certificates folder = (vide)
````

### Installation des images à distance

Appel depuis WSL2 :
````shell
cd [projet]/moodle/build
docker -H [IP hôte local WSL]:2375 build -t moodle_local:latest .
cd [projet]/trax/build
docker -H [IP hôte local WSL]:2375 build -t trax_local:latest .
````

### Exécution des containers
1. Sur la machine HyperV : créer le fichier .env à partir du .env.example, puis éditer les valeurs.

2. Générer le docker-compose.yml avec la commande :
````shell
./launcher.sh config
````
3. Lancer les containers :
````shell
./launcher.sh start
````

