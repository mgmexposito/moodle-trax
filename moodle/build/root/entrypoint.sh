#!/bin/bash

if [[ ! -f /var/www/html/moodle/install.php ]];then
  if [[ $(echo $MDL_VERSION | grep -Eo '[0-9]{2,3}') == $MDL_VERSION ]]; then
    MDL_VERSION="MOODLE_${MDL_VERSION}_STABLE"
  fi
  if [ "$(ls -A te)" ];then
    echo "Moodle directory is not empty, cleaning it..."
    rm -rf /var/www/html/moodle/*
    rm -rf /var/www/html/moodle/.*
  fi
  echo "Cloning repo, branch: $MDL_VERSION ..."
  git clone -b $MDL_VERSION --single-branch git://git.moodle.org/moodle.git /var/www/html/moodle
fi

### Pre install: Ownership on moodle code and config php
chown -R www-data:www-data /var/www/html/moodle/

echo "Configuring PHP..."
sed -i 's/;max_input_vars = 1000/max_input_vars = 5000/g' /usr/local/etc/php/php.ini-production
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 300M/g' /usr/local/etc/php/php.ini-production
sed -i 's/post_max_size = 8M/post_max_size = 300M/g' /usr/local/etc/php/php.ini-production
mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/conf.d/php.ini
###
### Install
# Generate conf file
if [ ! -f /var/www/html/moodle/config.php ]; then
  echo "php /var/www/html/moodle/admin/cli/install.php --chmod=0777 --dataroot=/var/www/moodledata --lang=$MDL_LANG --wwwroot=https://$MOODLE_DN --dbtype=$MDL_DB_TYPE --dbhost=$MDL_DB_HOST --dbname=$MDL_DB_NAME --dbuser=$MDL_DB_USER --dbpass=$MDL_DB_PASS --dbport=$MDL_DB_PORT --adminuser=$MDL_ADMIN_NAME --adminpass=$MDL_ADMIN_PASS --agree-license --non-interactive --allow-unstable --fullname=$MDL_FULL_NAME --shortname=$MDL_SHORT_NAME --skip-database"

  su - www-data -s /bin/bash -c "php /var/www/html/moodle/admin/cli/install.php --chmod=0777 --dataroot=/var/www/moodledata --lang=$MDL_LANG --wwwroot=https://$MOODLE_DN --dbtype=$MDL_DB_TYPE --dbhost=$MDL_DB_HOST --dbname=$MDL_DB_NAME --dbuser=$MDL_DB_USER --dbpass=$MDL_DB_PASS --dbport=$MDL_DB_PORT --adminuser=$MDL_ADMIN_NAME --adminpass=$MDL_ADMIN_PASS --agree-license --non-interactive --allow-unstable --fullname=$MDL_FULL_NAME --shortname=$MDL_SHORT_NAME --skip-database"
  # Adding sslproxy options to moodle's conf file to make it handle traefik
  sed -i '/0777;/i $CFG->sslproxy = true;' /var/www/html/moodle/config.php
fi

# Install database with generated conf file
if [[ -f /var/www/html/moodle/config.php ]] && [[ $(cat /var/www/html/moodle/config.php | grep '#db_installed#' 2>/dev/null) == "" ]]; then
  echo "Install database..."
  su - www-data -s /bin/bash -c "php /var/www/html/moodle/admin/cli/install_database.php --agree-license --adminpass=$MDL_ADMIN_PASS"
  echo '#db_installed#' >>/var/www/html/moodle/config.php
fi

# Good rights to make moodle work
chown www-data:www-data /var/www/html/moodle/config.php
chmod 750 /var/www/html/moodle/config.php
# Cron that is essential to moodle
echo "Setup cron..."
touch /var/log/cron.log
chown www-data:www-data /var/log/cron.log
su - www-data -s /bin/bash -c "(crontab -l 2>/dev/null; echo '*/1 * * * * /usr/local/bin/php /var/www/html/moodle/admin/cli/cron.php > /var/log/cron.log') | crontab -"
service cron restart
###

if [ "$MDL_PHP_DEBUG" = "true" ]; then
  echo "Setup Xdebug..."
  if [ ! -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ]; then
    apt update
    apt install -y iproute2
    pecl install xdebug
    docker-php-ext-enable xdebug
    echo 'xdebug.remote_port=9003' >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    echo 'xdebug.mode=debug' >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    echo 'xdebug.start_with_request=1' >>/usr/local/etc/php/conf.d/docker-php-ext-lxdebug.ini
    echo 'xdebug.client_host='$(/sbin/ip route | awk '/default/ { print $3 }') >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    echo 'xdebug.idekey=PHPSTORM' >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
  fi
else
  echo "No Xdebug."
fi

### Conf apache
echo "Configuring apache..."
echo DocumentRoot "/var/www/html/moodle/" >>/etc/apache2/apache2.conf
a2enmod rewrite

cat <<'EOF' >/etc/apache2/conf-enabled/moodle.conf
<VirtualHost *:80>
     DocumentRoot /var/www/html/moodle
     <Directory /var/www/html/moodle>
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        allow from all
     </Directory>
</VirtualHost>
EOF
###

### Clearing variables
echo "Clearing environnement variables..."
MDL_DB_HOST=''
MDL_DB_USER=''
MDL_DB_PASS=''
MDL_DB_PORT=''
MDL_ADMIN_NAME=''
MDL_ADMIN_PASS=''

TRAX_DB_TYPE=''
TRAX_DB_HOST=''
TRAX_DB_NAME=''
TRAX_DB_PORT=''
TRAX_DB_USER=''
TRAX_DB_PASS=''
###

apache2-foreground
