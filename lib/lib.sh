usage() {
  echo -e "Arguments: prepare / docker_install / config / build / start / stop / backup \nOptions: \n\t-h --> Show this message \n\t-v --> Verbose option\n\t-f --> Force root execution\n\nDeploy from fresh server:
    launcher.sh prepare (git, fail2ban)
    launcher.sh docker_install (docker, docker-compose)
    launcher.sh config (.env interactive preparation if not exists, then creates docker-compose.yml)
    launcher.sh build (docker images build)
    launcher.sh start (configure certificates and launches containers)"
}
user_ask() {
  # That function is used to get user answer and return it to standard output
  # It can be used for yes or no questions:
  #        In that case the first argument must be y or n which is the default answer
  # It can also be used for open questions:
  #         The first argument must be the default answer
  while true; do
    local answer=""
    if [[ $1 == "y" ]] || [[ $1 == "n" ]]; then
      echo "[${1^^}/$2]:"
      read answer
      if [[ ${answer,,} == "$1" ]] || [[ $answer == "" ]]; then
        return 1
      elif [[ ${answer} == "$2" ]]; then
        return 0
      fi
    else
      read answer
      if [[ $answer == "" ]]; then
        echo $1
        return 0
      else
        echo $answer
        return 1
      fi
    fi
  done
}
fill_env() {
  # Fill .env file
  # First argument must be the name of the environnement variable
  # Second is the value
  sudo sed -i "s/$1.*$/$1=$2/g" ${local_path}/.env
}
test_package() {
  # test if the package is installed on the system
  # First argument must be the command to test
  if [[ $test_str == "" ]];then
    case $(pack_man) in
      "yum")
       test_str="yum info"
      ;;
     "apt-get" | "apt")
       test_str="dpkg -s"
     ;;
      "dnf")
       test_str="dnf list $1"
     ;;
      *)
       echo "Unknown package manager: Cannot check package"
       exit 4
     ;;
    esac
  fi
  if eval "$test_str $1" &> /dev/null;then
    return 0
  else
    return 1
  fi
}
pack_man() {
  # Return the package manager of the system
  pack_list='yum apt-get dnf apt'
  if [[ -n $pack_man ]]; then
    echo $pack_man
  else
    for i in $pack_list; do
      if which $i &> /dev/null; then
        pack_man="$i"
        echo $pack_man
        break
      fi
    done
  fi
}
install_package() {
  # Install the packages given as arguments
  if [[ ! -z $verbose ]]; then
    quiet=""
  else
    quiet="-q -y"
  fi
  packages=("$@")
  eval "sudo $(pack_man) update $quiet && sudo $(pack_man) upgrade $quiet"
  for i in "${!packages[@]}"; do
    if test_package ${packages[$i]}; then
      unset "packages[$i]"
    fi
    if [ ${#packages[@]} -eq 0 ];then
      return 0
    fi
  done
  eval "sudo $(pack_man) install ${packages[*]} $quiet"
  return 0
}
build_image() {
  # Build images
  source ${local_path}/.env
  echo "Building moodle image..."
  cd ${local_path}/moodle/build
  if [[ ! -z $verbose ]]; then
    docker build -t moodle_local:latest .
  else
    docker build -t moodle_local:latest . | grep Step
  fi

  echo -e "\nBuilding trax image..."
  cd ${local_path}/trax/build
  if [[ ! -z $verbose ]]; then
    docker build -t trax_local:latest .
  else
    docker build -t trax_local:latest . | grep Step
  fi

  if [ "$ENABLE_ELASTIC" == 1 ]; then
    echo -e "\nBuilding elastic image..."
    cd ${local_path}/elasticsearch
    if [[ ! -z $verbose ]]; then
      docker build -t elasticsearch_local:latest . --build-arg ELK_VERSION=$ELK_VERSION
    else
      docker build -t elasticsearch_local:latest . --build-arg ELK_VERSION=$ELK_VERSION | grep Step
    fi
  
    echo -e "\nBuilding kibana image..."
    cd ${local_path}/kibana
    if [[ ! -z $verbose ]]; then
      docker build -t kibana_local:latest . --build-arg ELK_VERSION=$ELK_VERSION
    else
      docker build -t kibana_local:latest . --build-arg ELK_VERSION=$ELK_VERSION | grep Step
    fi

    echo -e "\nBuilding logstash image..."
    cd ${local_path}/logstash
    if [[ ! -z $verbose ]]; then
      docker build -t logstash_local:latest . --build-arg ELK_VERSION=$ELK_VERSION
    else
      docker build -t logstash_local:latest . --build-arg ELK_VERSION=$ELK_VERSION | grep Step
    fi
  fi
}
restart_container(){
  echo "Restarting containers..."
  cd $local_path
  mkdir -p $HOSTPIPE_DIR
  docker-compose down && docker-compose up -d --build
}
start_container() {
  docker network create ext_traefik 2>/dev/null
  echo "Starting containers..."
  cd $local_path
  # If needed, install a named pipe on the host, for containers calls
  # ./lib/install_execpipe.sh lib/execpipe.sh $HOSTPIPE_DIR
  # If not, pipe dir is created empty :
  mkdir -p $HOSTPIPE_DIR
  docker-compose down && ./launcher.sh config && docker-compose up -d --build
}
stop_container() {
  echo "Stoping containers..."
  cd $local_path
  docker-compose down
}
docker_install() {
  # Install docker on the system

  # Handle rocky linux
  if [[ $(cat /etc/os-release | grep -E "^ID=" | cut -d "=" -f 2) == \"rocky\" ]]; then
    sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
    install_package docker-ce docker-ce-cli containerd.io
  else
    cd /tmp
    sudo curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    sudo rm get-docker.sh
  fi
  # Install docker-compose
  sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
  sudo chmod +x /usr/bin/docker-compose
  sudo systemctl enable docker
  sudo systemctl start docker
  sudo usermod -aG docker $USER
  sudo reboot
}
configure() {
  if [ ! -f ${local_path}/.env ]; then
    sudo cp ${local_path}/example.env ${local_path}/.env
    echo "Welcome to docker-moodletrax config assistant !"
    echo "Enable moodle?"
    user_ask y n
    en_mdl=$?
    fill_env ENABLE_MDL $en_mdl
    echo "Enable trax?"
    user_ask y n
    en_trax=$?
    fill_env ENABLE_TRAX $en_trax
    user_ask y n
    en_letsencrypt=$?
    fill_env LETS_ENCRYPT_ENABLE $en_letsencrypt
    if [[ $en_mdl -eq 1 ]]; then
      while true; do
        for i in $(ip a | grep inet | grep -Eo '([0-9]{1,3}\.){3}[0-9]{1,3}'); do
          if [[ $(host $mdl_dn 2>/dev/null | awk '{print $4}' | grep $i 2>/dev/null) != "" ]]; then
            break 2
          fi
        done
        echo "Moodle front (The DNS record has to match ip address of local network card) :"
        mdl_dn=$(user_ask "error")
      done
      fill_env MOODLE_DN $mdl_dn
      echo "Moodle version? (Branch name)"
      mdl_version=$(user_ask "311")
      fill_env MDL_VERSION $mdl_version
      echo "Admin password?"
      mdl_pass=$(user_ask "change_me")
      fill_env MDL_ADMIN_PASS $mdl_pass
      echo "Moodle site full name?"
      mdl_fullname=$(user_ask "My moodle site")
      fill_env MDL_FULL_NAME "\'\"$mdl_fullname\"\'"
      echo "Moodle site short name?"
      mdl_shortname=$(user_ask "Moodle")
      fill_env MDL_SHORT_NAME "\'\"$mdl_shortname\"\'"
      echo "Moodle database password?"
      mdl_dbpass=$(user_ask "InS3cu4e_P477w04D")
      fill_env MDL_DB_PASS "\"$mdl_dbpass\""
    fi
    if [[ $en_trax -eq 1 ]]; then
      while true; do
        for i in $(ip a | grep inet | grep -Eo '([0-9]{1,3}\.){3}[0-9]{1,3}'); do
          if [[ $(host $trax_dn 2>/dev/null | awk '{print $4}' | grep $i 2>/dev/null) != "" ]]; then
            break 2
          fi
        done
        echo "Trax front (The DNS record has to match ip address of local network card) :"
        trax_dn=$(user_ask "error")
      done
      fill_env TRAX_DN $trax_dn
      echo "Trax version? (Branch name)"
      trax_version=$(user_ask "beta4")
      fill_env TRAX_VERSION $trax_version
      echo "Trax app name ?"
      trax_appname=$(user_ask "My Trax")
      fill_env TRAX_APP_NAME "\'\"$trax_appname\"\'"
      echo "Trax database password?"
      trax_dbpass=$(user_ask "InS3cu4e_P477w04D")
      fill_env TRAX_DB_PASS "\"$trax_dbpass\""
      echo "Trax admin e-mail?"
      trax_adminmail=$(user_ask "test@test.fr")
      fill_env TRAX_ADMIN_MAIL "\"$trax_adminmail\""
      echo "Trax admin password?"
      trax_adminpass=$(user_ask "InS3cu4e_P477w04D")
      fill_env TRAX_ADMIN_PASS "\'"$trax_adminpass"\'"
    fi
    source ${local_path}/.env
  fi
  echo -e "version: '3.2'\nservices:" | sudo tee ${local_path}/docker-compose.yml >/dev/null
  cat ${local_path}/compose_builder/traefik.yml | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  if [[ "$ENABLE_MDL" == "1" ]]; then
    cat ${local_path}/compose_builder/moodle.yml | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  if [[ "$ENABLE_TRAX" == "1" ]]; then
    cat ${local_path}/compose_builder/trax.yml | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  if [[ "$ENABLE_ELASTIC" == "1" ]]; then
    cat ${local_path}/compose_builder/elastic.yml | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  else
    # Delete "- moodle2elastik" network in Moodle docker part
    sed -i '/- moodle2elastik/d' ${local_path}/docker-compose.yml
  fi
  echo -e "networks:\n   ext_traefik:\n      external: true" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  if [[ "$ENABLE_MDL" == "1" ]]; then
    echo -e "   net_moodle:\n      driver: bridge\n      internal: true" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  if [[ "$ENABLE_TRAX" == "1" ]]; then
    echo -e "   net_trax:\n      driver: bridge\n      internal: true" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  echo -e "   moodle2trax:\n      driver: bridge\n      internal: true" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  if [[ "$ENABLE_ELASTIC" == "1" ]]; then
    echo -e "   elk:\n      driver: bridge" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
    echo -e "   moodle2elastik:\n      driver: bridge\n      internal: true" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  echo "volumes:" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  if [[ "$ENABLE_MDL" == "1" ]]; then
    echo -e "   moodle_db:\n      driver: local" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  if [[ "$ENABLE_MDL" == "1" ]]; then
    if [ -z ${MDL_VOLUME_BIND_CODE+x} ]; then # $MDL_VOLUME_BIND_CODE is not set -> default volumes
      echo -e "   moodle_code:\n      driver: local\n   moodle_data:\n      driver: local\n" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
    else # $MDL_VOLUME_BIND_CODE is  set -> bind mounts
       sed "s+- moodle_code:/var/www/html/moodle/+- type: bind\n            source: $MDL_VOLUME_BIND_CODE\n            target: /var/www/html/moodle/\n          - type: bind\n            source: $MDL_VOLUME_BIND_DATA\n            target: /var/www/moodledata/+g" ${local_path}/docker-compose.yml | sudo tee ${local_path}/docker-compose.yml >/dev/null
       sudo sed  -i '/moodle_data:\/var\/www\/moodledata/d' ${local_path}/docker-compose.yml
    fi
  fi
  if [[ "$ENABLE_TRAX" == "1" ]]; then
    echo -e "   trax_code:\n      driver: local\n   trax_db:\n      driver: local" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  if [[ "$ENABLE_ELASTIC" == "1" ]]; then
    echo -e "   elasticsearch:\n" | sudo tee -a ${local_path}/docker-compose.yml >/dev/null
  fi
  sudo mkdir ${local_path}/backup/moodle 2>/dev/null
  sudo mkdir ${local_path}/backup/trax 2>/dev/null
  sudo touch ${local_path}/traefik/log/access.log 2>/dev/null
  sudo touch ${local_path}/traefik/log/traefik.log 2>/dev/null

  echo -e "\nConfiguring traefik certificates..."
  if [[ "$LETS_ENCRYPT_ENABLE" == "true" ]]; then
    sudo sed -i "s/&acme_mail&/$LETS_ENCRYPT_WARN_EMAIL/g" ${local_path}/traefik/traefik.toml
  else
    # Désactivation de Let'sEncrypt
    echo -e "\nDeactivating Let's Encrypt..."
    sudo sed -i '/- \"traefik\.http\.routers\.moodle-websecured\.tls\.certresolver=letsencrypt\"/d' ${local_path}/docker-compose.yml
    sudo sed -i '/- \"traefik\.http\.routers\.trax-websecured\.tls\.certresolver=letsencrypt\"/d' ${local_path}/docker-compose.yml
  fi
}
backup() {
  # Import backup configuration
  source ${local_path}/.backup-secrets || (
    cp ${local_path}/backup-secrets.example ${local_path}/.backup-secrets
    source ${local_path}/.backup-secrets
  )
  date=$(date +"%Y-%m-%d_%H-%m-%S")
  mdldb_id=$(docker ps | grep "$(docker-compose ps --services)" | grep moodle_db | awk '{print $1}')
  mdl_id=$(docker ps | grep "$(docker-compose ps --services)" | grep moodle_local | awk '{print $1}')

  traxdb_id=$(docker ps | grep "$(docker-compose ps --services)" | grep trax_db | awk '{print $1}')
  trax_id=$(docker ps | grep "$(docker-compose ps --services)" | grep trax_local | awk '{print $1}')

  cd $local_path
  mkdir /tmp/tmp_backup/

  ###
  if [[ ! -z $verbose ]]; then
    echo "Backing up moodle..."
  fi
  docker exec $mdldb_id pg_dump --username=$(cat ${local_path}/.env | grep MDL_DB_NAME | cut -d = -f2) --file="/tmp/moodle-db_${date}.sql" -C -d $(cat ${local_path}/.env | grep MDL_DB_NAME | cut -d = -f2)
  sudo docker cp $mdldb_id:/tmp/moodle-db_${date}.sql /tmp/tmp_backup/
  docker exec $mdl_id tar -cf /tmp/moodle_code-data_${date}.tar /var/www/html/moodle /var/www/moodledata
  sudo docker cp $mdl_id:/tmp/moodle_code-data_${date}.tar /tmp/tmp_backup/
  tar -czf ${local_path}/backup/moodle/moodle_${date}.tar.gz /tmp/tmp_backup/*
  sudo rm -rf /tmp/tmp_backup/*
  ##
  if [[ ! -z $verbose ]]; then
    echo "Backing up trax..."
  fi
  docker exec $traxdb_id pg_dump --username=$(cat ${local_path}/.env | grep TRAX_DB_USER | cut -d = -f2) --file="/tmp/trax-db_${date}.sql" -C -d $(cat ${local_path}/.env | grep TRAX_DB_NAME | cut -d = -f2)
  sudo docker cp $traxdb_id:/tmp/trax-db_${date}.sql /tmp/tmp_backup/
  docker exec $trax_id tar -cf /tmp/trax_code_${date}.tar /var/www/html/trax
  sudo docker cp $trax_id:/tmp/trax_code_${date}.tar /tmp/tmp_backup/
  tar -czf ${local_path}/backup/trax/trax_${date}.tar.gz /tmp/tmp_backup/*
  sudo rm -rf /tmp/tmp_backup
  ##
  if [[ $CIPHERKEY != "" ]]; then
    if [[ ! -z $verbose ]]; then
      echo "Cipherkey found... Ciphering data..."
    fi
    openssl enc -e -aes-256-cbc -in ${local_path}/backup/moodle/moodle_${date}.tar.gz -out ${local_path}/backup/moodle/moodle_${date}.tar.gz.ciph -pass pass:"$(<${local_path}/.cipherkey)"
    openssl enc -e -aes-256-cbc -in ${local_path}/backup/trax/trax_${date}.tar.gz -out ${local_path}/backup/trax/trax_${date}.tar.gz.ciph -pass pass:"$(<${local_path}/.cipherkey)"
    rm -f ${local_path}/backup/moodle/moodle_${date}.tar.gz
    rm -f ${local_path}/backup/trax/trax_${date}.tar.gz
  else
    if [[ ! -z $verbose ]]; then
      echo "No cipherkey found..."
    fi
  fi

  if [[ $ENABLE_FTP_BACKUP -eq 1 ]]; then
    ftp -n $FTP_HOST <<END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASS
put ${local_path}/backup/moodle/moodle_${date}.tar.gz* ${REMOTE_DIR_MDL}/
put ${local_path}/backup/trax/trax_${date}.tar.gz* ${REMOTE_DIR_TRAX}/
END_SCRIPT
  fi
  ##
  for i in $(ls ${local_path}/backup/moodle || ls ${local_path}/backup/trax); do
    date_file=$(echo $i | grep -Eo "[0-9]{4}(-[0-9]{2}){2}")
    dif_date=$(echo "$((($(date +%s) - $(date -d $date_file '+%s')) / 86400))")
    if [ $dif_date -ge $TIME_TO_KEEP ]; then
      rm ${local_path}/backup/moodle/moodle_${date_file}* 2>/dev/null
      rm ${local_path}/backup/trax/trax_${date_file}* 2>/dev/null
      if [[ $ENABLE_FTP_BACKUP -eq 1 ]]; then
        ftp -n $FTP_HOST <<END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASS
delete ${REMOTE_DIR_MDL}/moodle_${date_file}*
delete ${REMOTE_DIR_TRAX}/trax_${date_file}*
END_SCRIPT
      fi
    fi
  done
}
prepare_env() {
  # Install basic packages
  install_package epel-release
  install_package git fail2ban-server bind-utils fail2ban-firewalld
  sudo tee /etc/fail2ban/jail.local &> /dev/null <<'EOF'
[DEFAULT]
# Ban hosts for one hour:
bantime = 3600

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true
EOF
}
