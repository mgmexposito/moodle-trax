#!/bin/bash

execpipescript="$1"
pipedir="$2"
echo "Installing crontab for $PWD/$execpipescript"
if [[ $(crontab -l | egrep -v "^(#|$)" | grep -q "$PWD/$execpipescript"; echo $?) == 1 ]]
then
    crontab -l > cron.tmp
    echo "@reboot $PWD/$execpipescript $pipedir" >> cron.tmp
    cat cron.tmp | crontab -
    rm cron.tmp
fi

echo "Checking if $PWD/$execpipescript is started"
if pidof -x "$PWD/$execpipescript" >/dev/null;
then
    echo "Process already running"
else
    $PWD/$execpipescript $pipedir &
    echo "Process started"
fi
